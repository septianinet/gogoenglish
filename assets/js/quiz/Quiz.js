"use strict";


/**
 * Quiz
 * @param {string} name
 * @param {array} questions
 * @param {object} options
 *
 * options object supports these keys:
 *     shuffle {boolean} should the questions be shuffled before rendered?
 */
function Quiz(name, selector, questions, options) {
    this.name = name;
    this.selector = selector;
    this.questions = questions;
    this.options = options || {};
    this.create();
}


Quiz.prototype.getName = function () {
    return this.name;
}


Quiz.prototype.setName = function (name) {
    this.name = name;
    return this.name;
};

Quiz.prototype.getSelector = function () {
    return this.selector;
}


Quiz.prototype.setSelector = function (selector) {
    this.selector = selector;
    return this.selector;
};


Quiz.prototype.getQuestions = function () {
    return this.questions
};


Quiz.prototype.setQuestions = function (questions) {
    this.questions = questions;
    return this.questions;
};


Quiz.prototype.addQuestion = function (question) {
    return this.questions.push(question);
};


Quiz.prototype.removeQuestion = function (question) {
    console.info("Remove question from the quiz");
    return this.questions;
};


Quiz.prototype.create = function () {
    let form = document.createElement("form"),
        submit = document.createElement("input"),
        quizBox = document.querySelector(this.selector)
    const self = this;


    submit.type = "submit";
    submit.value = "Submit quiz";

    form.addEventListener("submit", function (e) {
        e.preventDefault();
        self.submit();
    })

    for (var i in this.questions) {
        let number = +i + 1;
        form.appendChild(this.questions[i].render(number, this.options.shuffle || false));
    }

    form.appendChild(submit);
    quizBox.appendChild(form);
};


Quiz.prototype.submit = function () {
    var score = 0,
        right = 0

    for (var q of this.getQuestions()) {
        var choice = null;

        // validate
        for (var o of q.getOptions()) {
            if (o.isSelected()) {
                choice = o;
            }
        }

        // evaluate
        if (!choice) { // skip evaluation
            continue;
        }

        if (!q.isRight(choice)) {
            // q.renderWrong();
        } else {
            q.renderRight();
            right += 1;
            score = right * 10;
        }
    }
    // show result
    var result = new Result(score, this.selector).render();
};
