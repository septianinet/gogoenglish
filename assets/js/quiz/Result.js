"use strict";


/**
 * @param {integer} missing
 * @param {integer} right
 * @param {integer} wrong
 */
function Result(score, selector) {
    this.selector = selector;
    this.html = document.createElement("table");
    this.html.classList.add('quiz-result');

    var labels = ["Score"],
        tr2 = document.createElement('tr'),
        tr = document.createElement("tr");

    var label = document.createElement("td"),
        score = document.createElement("td");

        var label2 = document.createElement("td");
        label2.setAttribute('colspan', 2)
        label2.style.textAlign = 'center'
    label.className = "score-label";
    score.className = "score-score";

    label.innerHTML = "Your Score";
    score.innerHTML = arguments[0];

    label2.className = "score-label";
    if (arguments[0] >= 20) {
        label2.innerHTML = "Excellent!";
    } else if (arguments[0] >= 10) {
        label2.innerHTML = "Nice!";
    } else {
        label2.innerHTML = "Try Again!";
    }

    tr.appendChild(label);
    tr.appendChild(score);

    tr2.appendChild(label2);

    this.html.append(tr, tr2);
}


Result.prototype.render = function () {
    var old = document.querySelector(`${this.selector} .quiz-result`);
    if (old) {
        document.querySelector(this.selector).removeChild(old);
    }
    document.querySelector(this.selector).appendChild(this.html);

    return this;
};
