"use strict";

const question21 = {
    text: `What does he say to get his friend's attention?`,
    choices: [
        "a. I'll make something delicious for you all.",
        "b. May I have your attention please?",
        "c. Alright.",
        "d. Do you understand me?",
    ],
};
const question22 = {
    text: `How does his friend respond it?`,
    choices: [
        "a. All right",
        "b. Fine",
        "c. Yes, sure",
        "d. Ok",
    ],
};
const question23 = {
    text: `Which one is the expression of respond understanding on the dialogue?`,
    choices: [
        "a. Yes, we understand",
        "b. I get",
        "c. I don't understand",
        "d. Not really",
    ],
};
const question24 = {
    text: `Which one is the expression of appreciation to others?`,
    choices: [
        "a. Your burger tastes nice",
        "b. Your burger is delicious",
        "c. Your burger is yummy",
        "d. Your burger is tasty",
    ],
};

const question25 = {
    text: `What is Andy's opinion about the meal?`,
    choices: [
        "a. I think the burger tastes good",
        "b. I think the burger doesn't good taste",
        "c. I think the burger tastes nice",
        "d. I think the burger doesn't like Indonesian burger",
    ],
};


const questions2 = [
    new Question(question21.text, question21.choices, 1),
    new Question(question22.text, question22.choices, 2),
    new Question(question23.text, question23.choices, 0),
    new Question(question24.text, question24.choices, 3),
    new Question(question25.text, question25.choices, 0),
];

const quizSectionTwo = new Quiz("", ".section-2", questions2, { shuffle: false });
