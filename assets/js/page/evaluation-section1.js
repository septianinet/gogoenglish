"use strict";

const question1 = {
    text: `What kind of this expression “attention please?”`,
    choices: [
        "a. Asking opinion",
        "b. Giving attention",
        "c. Giving prohibition",
        "d. Asking attention",
    ],
};
const question2 = {
    text: `These are asking for attention expressions, except?`,
    choices: [
        "a. Are you ready to study?",
        "b. Hello?",
        "c. I suggest to you",
        "d. Attention please",
    ],
};
const question3 = {
    text: `
                            Justine	: This enzyme helps our body digest food.<br>
                            Azriel	: Mmm, I don't get it.<br>
                            Justine	: (3)…<br>
                            Azriel	: Well, I'm not sure I know what you mean.<br>
                            Justine	: Do you want me to repeat my explanation?<br>
                            Azriel	: Yes, please.<br>
                            Choose the right answer to complete the dialogue above!<br>
                        `,
    choices: [
        "a. Can you tell me once again?",
        "b. Do you know what I’m talking about?",
        "c. Do you believe it?",
        "d. What do you mean?",
    ],
};

const question4 = {
    text: `
                            Shinta	: Don't you understand of what Mrs.Tiwi said?<br>
                            Tasya	: Yes, she said that we will speak English in our class.<br>
                            What is Shinta doing?
                        `,
    choices: [
        "a. Checking understanding",
        "b. Asking opinion",
        "c. Asking attention",
        "d. Appreciating works",
    ],
};


const question5 = {
    text: `
            Anwar		: Mr. Rahmat, I've finished my task. Would you mind checking if all my answers are correct?<br>
            Mr. Rahmat	: I've checked it. Excellent! I appreciate your good work, Anwar.<br>
            Anwar		: …<br>
            Fill the gap with the best answer to complete the dialoge above!<br>
                        `,
    choices: [
        "a. Never mind",
        "b. Thank you",
        "c. You're welcome",
        "d. Mention it",
    ],
};

const question6 = {
    text: `
            Debrina	: Look at the handsome boy over there!<br>
            Jeanny		: …
                        `,
    choices: [
        "a. Wow. How beautiful she is.",
        "b. How do you do",
        "c. Yeah! How hansdome he is",
        "d. Thank you",
    ],
};
const question7 = {
    text: `
            Rinneke	: <u>Your handwriting is clear and beautiful, Adele.</u><br>
            Adele		: Thanks.<br>
            The underlined sentence is an expression of …
                        `,
    choices: [
        "a. Getting attention",
        "b. Checking someone's understand",
        "c. Giving suggestion",
        "d. Appreciation to others",
    ],
};
const question8 = {
    text: `
            Ara	: What do you think about my new hairstyle? Am I beautiful with it?<br>
            Eca	: I think you are beautiful with it.<br>
            Ara	: And you are beautiful too, Eca.<br>
            Eca	: Thanks my twin.<br>
            The words “what do you think about my new hairstyle?” are the expression of …
                        `,
    choices: [
        "a. Asking opinion",
        "b. Showing appreciation",
        "c. Giving opinion",
        "d. Asking attention",
    ],
};
const question9 = {
    text: `
            Kate	: What's your opinion abut this novel?<br>
            Liam	: I like it. It is an interesting story<br>
            From the dialogue we can conclude that …
                        `,
    choices: [
        "a. Liam doesn't like the novel",
        "b. Liam is giving his opinion",
        "c. Kate agrees with Liam's opinion",
        "d. Liam is asking Kate's opinion",
    ],
};
const question10 = {
    text: `
            Irene	: I think our city is safe from the virus, so we can go around.<br>
            Anggar	: But I don't think so, … our city is not save. We must stay at home at least in lockdown time.<br>
            Fill the gap with the best answer to complete the dialoge above!
                        `,
    choices: [
        "a. In my opinion",
        "b. See you next time",
        "c. She forgets it",
        "d. I'm with you",
    ],
};

const questions = [
    new Question(question1.text, question1.choices, 3),
    new Question(question2.text, question2.choices, 2),
    new Question(question3.text, question3.choices, 1),
    new Question(question4.text, question4.choices, 0),
    new Question(question5.text, question5.choices, 1),
    new Question(question6.text, question6.choices, 2),
    new Question(question7.text, question7.choices, 3),
    new Question(question8.text, question8.choices, 0),
    new Question(question9.text, question9.choices, 1),
    new Question(question10.text, question10.choices, 0),
];

const quizSectionOne = new Quiz("", ".section-1", questions, { shuffle: false });
